3.

  (function () {
    console.log(1);
    setTimeout(function () { console.log(2) }, 1000);
    setTimeout(function () { console.log(3) }, 0);
    console.log(4);
  })();

1
4
3
2

8.
const a = { b: 1, c: 2 };
const b = { a: 1, b: 2 };
const c = { a: 1, b: 2 };

c[a] = a;
c[b] = b;

c[b] ???

  c - { a: 1, b: 2, '[object Object]': { a: 1, b: 2 } };

9.
const arr = [1, 2, 3];

arr[Symbol.iterator] = function () {
  this.i = this.length - 1;
  this.next = () => ({ value: this[this.i], done: this.i-- <= -1 });
  return this;
}

console.log(...arr); - logs[3, 2, 1]

10.
sum(1)(2) === 3

const sum = a => b => a + b;

11.
  + sum(1)(2)()()()(-3)()()(7) === 7

const sum = (initialArg = 0) => {
  let acc = 0;

  const resCallback = (arg = 0) => {
    acc += arg;
    return resCallback;
  };

  resCallback.toString = resCallback.valueOf = () => acc;

  return resCallback;
}

+sum(5)()()()(-8)()()()()(3)();

12. 
  arr[-1] the same with array[array.length - 1]

p = new Proxy([], {
    get: function(target, name) {
        return 'HI';
    }
    set: ...
})

console.log(p[123]);

13.
Implement a function compressing input string using run-length encoding algorithm:
abbcccdddd -> a1b2c3d4
aaaabbbccd -> a4b3c2d1

function* compress(iterator) {
    let count = 0, current, previous;
    for (current of iterator) {
        if (count) {
            if (current === previous) {
                count++;
                continue;
            } else {
                yield `${current}${count}`;
            }
        }
        count = 1;
        previous = current;
    }
    if (count) {
        yield `${current}${count}`;
    }
}

function join(iterator) {
    let result = '';
    for (const item of iterator) {
        result += item;
    }
    return result;
}

join(compress('abbcccdddd'));
join(compress('aaaabbbccd'));

// Another solution

const target = new String("assddd");

target[Symbol.iterator] = function* () {
  let currentCharacter = this[0];
  let charsAmount = 0;

  for (let index = 0; index < this.length + 1; index++) {
    if (currentCharacter !== this[index]) {
      yield currentCharacter + charsAmount;
      currentCharacter = this[index];
      charsAmount = 1;
    } else {
      charsAmount++;
    }
  }
};

console.log([...target]);

14.
Use yield instead awaitt

const wait = (value, time = 1e3) =>
  new Promise((res) => setTimeout(() => res(value), time));

const generator = function* () {
  const a = yield wait(1, 1e3);
  console.log("runner", a);
  const b = yield wait(2, 2e3);
  console.log("runner", b);
};

const runner = (generator) => {
  const iterator = generator();

  const run = (args) => {
    const currentIt = iterator.next(args);
    if (!currentIt.done) {
      console.log("inside");
      return Promise.resolve(currentIt.value).then(run);
    }
    return currentIt.value;
  };

  run();
};

runner(generator);
